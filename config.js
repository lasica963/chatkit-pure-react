const tokenUrl = "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/06030335-a769-41ae-b7e1-e8f39746abe1/token";
const instanceLocator = "v1:us1:06030335-a769-41ae-b7e1-e8f39746abe1";

export {tokenUrl, instanceLocator};

